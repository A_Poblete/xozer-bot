
import discord
from discord.ext import commands
import requests
import re
#Bot para discord :D
bot = commands.Bot(command_prefix=':::', description= "este es el bot de warframe alerts" )

@bot.command()
async def hola(ctx):
   await ctx.send('hey muchachos ustedes escucharon el rempalago!!')

@bot.command()
#https://api.warframestat.us/pc/cetusCycle
async def cetus(ctx):
    embed = discord.Embed(title="Cetus")
    r = requests.get('https://api.warframestat.us/pc/cetusCycle')
    contenido= r.json() # diccionario?
    tiempo = contenido['timeLeft']
    estado = contenido['isDay']
    embed.add_field(name="tiempo restante", value=tiempo)
    embed.set_thumbnail(url="./luna.png")
    if estado :
      embed.add_field(name="Estado del ciclo ",  value="el sol brilla y los cazadores descansan")
      embed.set_thumbnail(url="https://png2.kisspng.com/sh/a97125989aa9dcd0e7ac60511bd8f533/L0KzQYm3VMA4N5hufZH0aYP2gLBuTfNwdaF6jNd7LXnmf7B6TgJme5ZmitVxLXHxdH7rhgZmdJD1hdd3dD3mfLr3TfFzfF54h942NXHmSIm6gsI4aWlmUKU3OEW4R4iBVsAyPWM4SKs5M0W3RYG4Wb5xdpg=/kisspng-computer-icons-research-and-development-clip-art-sol-5ac883b27a8a83.8557786015230903545019.png")
    else:
      embed.add_field(name="Estado del ciclo ",  value="soltad los kavats nos vamos de cazeria")
      embed.set_thumbnail(url="https://cdn.icon-icons.com/icons2/865/PNG/512/Citycons_night_icon-icons.com_67936.png")

    await ctx.send(embed=embed)

@bot.command()
async def incursion(ctx):
    
    r = requests.get('https://api.warframestat.us/pc/sortie')
    content = r.json()
    faction = content['faction']
    timeLeft = content['eta']

    first = content['variants'][0]['missionType']
    firstConditon = content['variants'][0]['modifier']
    firstnode = content['variants'][0]['node']
    second = content['variants'][1]['missionType']
    secondConditon = content['variants'][1]['modifier']
    secondnode = content['variants'][1]['node']
    third = content['variants'][2]['missionType']
    thirdConditon = content['variants'][2]['modifier']
    thirdnode = content['variants'][2]['node']
    
    embed = discord.Embed( title = "Incursion Diaria")
    embed.color = discord.Color.dark_green()
    embed.description = "Faccion: "+ faction + "\n tiempo restante: "+ timeLeft 
    embed.set_thumbnail(url = "https://i.imgur.com/BQ82OeB.png")
    embed.add_field(name = "mision lvl :five::zero:", value = first , inline= True)
    embed.add_field(name = "condicion ", value=firstConditon, inline= True)
    embed.add_field(name =  "Ubicacion", value=firstnode, inline= True )
    embed.add_field(name = "mision lvl :seven::five:", value = second , inline= True)
    embed.add_field(name = "condicion ", value=secondConditon, inline= True)
    embed.add_field(name ="Ubicacion:", value=secondnode, inline= True )
    embed.add_field(name = "mision lvl :one::zero::zero:", value = third , inline= True)
    embed.add_field(name = "condicion ", value=thirdConditon, inline= True)
    embed.add_field(name ="Ubicacion:", value=thirdnode, inline= True )
    embed.set_footer(text= "Xozer bot Warframe")
    await ctx.send(embed = embed)

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Game(name="Warframe"))
    print('Rempalago online')
bot.run('my private key discord')
